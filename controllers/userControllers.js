const User = require("../models/User");


module.exports.checkNameExists = (reqBody) => {
	return User.find({name: reqBody.name}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.saveUser = (reqBody) => {
	let newUser = new User({
		name: reqBody.name,
		address: reqBody.address,
		contactNo: reqBody.contactNo,
		birthday: reqBody.birthday
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.updateName = (nameId, reqBody) => {
	let updatedName = {
		name: reqBody.name,
		name: reqBody.name,
		address: reqBody.address,
		contactNo: reqBody.contactNo,
		birthday: reqBody.birthday	
	};

	return User.findByIdAndUpdate(nameId, updatedName).then((item, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}

module.exports.archiveName = (nameId, reqBody) => {
	let archiveName = {
		isActive: reqBody.isActive
	};

	return User.findByIdAndUpdate(nameId, archiveName).then((name, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.getAllNames = () => {
	return User.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getName = (reqParams) => {
	return User.findById(reqParams).then(result => {
		return result;
	})
}