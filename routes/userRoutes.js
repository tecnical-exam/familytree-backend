const express = require("express");
const router = express.Router();
const UserController = require("../controllers/userControllers");

router.post("/checkName", (req, res) => {
	UserController.checkNameExists(req.body).then(result => res.send(result))
});

router.post("/saveUser", (req,res) => {
	UserController.saveUser(req.body).then(result => res.send(result));
});

router.put("/updateData/:nameId", (req,res) => {
	UserController.updateName(req.params.nameId,req.body).then(result => res.send(result));
});

router.put("/archiveName/:nameId", (req,res) => {
	UserController.archiveName(req.params.nameId,req.body).then(result => res.send(result));
})

router.get("/allNames", (req, res) => {
	UserController.getAllNames().then(result => res.send(result));
});

router.get("/:nameId", (req,res) => {
	UserController.getName(req.params.nameId).then(result => res.send(result));
});






module.exports = router;