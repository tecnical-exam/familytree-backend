const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, "name is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	address: {
		type: String
	},

	contactNo: {
		type: String
	},	

	birthday: {
		type: Date
	}

})

module.exports = mongoose.model("User", userSchema)